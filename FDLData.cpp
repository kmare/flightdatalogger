/*
 * FDLData.cpp
 *
 *  Created on: May 30, 2019
 *      Author: pioann
 */

#include "FDLData.h"

#include <time.h>

FDLData::FDLData() {
	ts = 0;
	lat = 0.0f;
	lon = 0.0f;
	speed = 0.0f;
}

FDLData::~FDLData() {

}

float FDLData::getLat() {
	return lat;
}

void FDLData::setLat(float lat) {
	// according to the IEEE standard, NaN values have the odd property that comparisons
	// involving them are always false.
	// That is, for a float f, f != f will be true only if f is NaN.
	if (lat == lat)
		this->lat = lat;
}

float FDLData::getLon() {
	return lon;
}

void FDLData::setLon(float lon) {
	if (lon == lon)
		this->lon = lon;
}

float FDLData::getPitch() {
	return pitch;
}

void FDLData::setPitch(float pitch) {
	if (pitch == pitch)
		this->pitch = pitch;
}

float FDLData::getRoll() {
	return roll;
}

void FDLData::setRoll(float roll) {
	if (roll == roll)
		this->roll = roll;
}

float FDLData::getSpeed() {
	return speed;
}

void FDLData::setSpeed(float speed) {
	if (speed == speed)
		this->speed = speed;
}

uint32_t FDLData::getTs() {
	return ts;
}

void FDLData::setTs(uint32_t ts) {
	this->ts = ts;
}

float FDLData::getYaw() {
	return yaw;
}

void FDLData::setYaw(float yaw) {
	if (yaw == yaw)
		this->yaw = yaw;
}

float FDLData::getYawRate() {
	return yaw_rate;
}

void FDLData::setYawRate(float yawRate) {
	if (yaw_rate == yaw_rate)
		yaw_rate = yawRate;
}

uint32_t FDLData::getUnixTime(uint8_t year, uint8_t month, uint8_t day,
		uint8_t hour, uint8_t min, uint8_t sec) {
    struct tm  timeinfo;
    uint32_t unixtime;

    timeinfo.tm_year = 2000 + year - 1900 + 30;
    timeinfo.tm_mon = month - 1;
    timeinfo.tm_mday = day;
    timeinfo.tm_hour = hour;
    timeinfo.tm_min = min;
    timeinfo.tm_sec = sec;

    unixtime = mktime(&timeinfo);
    FDLData::setTs(unixtime);
    return unixtime;
}
