/*
 * FDLData.h
 *
 *  Created on: May 30, 2019
 *      Author: pioann
 */

#ifndef FDLDATA_H_
#define FDLDATA_H_

#include <avr/io.h>
#include "libs/IMU/helper_3dmath.h"

class FDLData {
private:
	// gyroscope
	float yaw = 0;
	float pitch = 0;
	float roll = 0;
	float yaw_rate = 0;
	// GPS
	uint32_t ts = 0;
	float lat = 0;
	float lon = 0;
	float speed = 0;
public:
	FDLData();
	~FDLData();

	// quaternion: holds [w, x, y, z]
	Quaternion q;
	VectorFloat gravity;    // [x, y, z]            gravity vector
	float ypr[4]; // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

	uint32_t getUnixTime(uint8_t year, uint8_t month, uint8_t day,
			uint8_t hour, uint8_t min, uint8_t sec);

	float getLat();
	void setLat(float lat = 0);
	float getLon();
	void setLon(float lon = 0);
	float getPitch();
	void setPitch(float pitch = 0);
	float getRoll();
	void setRoll(float roll = 0);
	float getSpeed();
	void setSpeed(float speed = 0);
	uint32_t getTs();
	void setTs(uint32_t ts = 0);
	float getYaw();
	void setYaw(float yaw = 0);
	float getYawRate();
	void setYawRate(float yawRate = 0);

	const Quaternion& getQ() const {
		return q;
	}

	void setQ(const Quaternion& q) {
		this->q = q;
	}


};

#endif /* FDLDATA_H_ */
