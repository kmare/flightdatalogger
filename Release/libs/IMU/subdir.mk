################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../libs/IMU/I2Cdev.cpp \
../libs/IMU/MPU6050.cpp 

OBJS += \
./libs/IMU/I2Cdev.o \
./libs/IMU/MPU6050.o 

CPP_DEPS += \
./libs/IMU/I2Cdev.d \
./libs/IMU/MPU6050.d 


# Each subdirectory must supply rules for building sources it contributes
libs/IMU/%.o: ../libs/IMU/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


