/*
 * main.cpp
 *
 *  Created on: May 13, 2019
 *      Author: pioann
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

#include "config.h"

// I2Cdev and MPU6050 libraries
#include "libs/IMU/I2Cdev.h"
#include "libs/IMU/MPU6050_6Axis_MotionApps20.h"

// include some gps helper libraries
#include "libs/nmea/minmea.h"

// software one-wire serial
#include "libs/BasicSerial3.h"

// include our UART library
#include "libs/USARTWrapper.h"

// SD card stuff
extern "C" {
#include "libs/FatFs/ff.h"
#include "libs/MMC/mmc.h"
}

#include "FDLData.h"

// ICD definitions
#define MSG_PITCH 1
#define MSG_ROLL 2
#define MSG_YAW_RATE 3
#define MSG_LAT 4
#define MSG_LON 5
#define MSG_TIME 6
#define MSG_SPEED 7


#define RATE_1 100  // 1Hz  - 1000 ms
#define RATE_5 20   // 5Hz  - 200 ms
#define RATE_20 5   // 20Hz - 50 ms


// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun break-out and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

// MPU control/status variables
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus; // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion variables
// Quaternion q;           // [w, x, y, z]         quaternion container
// VectorFloat gravity;    // [x, y, z]            gravity vector

//float ypr[3]; // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
float yaw_current = 0;
float yaw_rate = 0;
float yaw_previous[20] = {0.0};
float quaternion_previous[4] = {0.0};
float roll_previous = 0.0;

volatile bool mpuInterrupt = false; // indicates whether MPU interrupt pin has gone high

// setup USART object
USARTWrapper* ser;

// filesystem stuff
FRESULT res; // return status after disk operations
FATFS fs;
DIR dir;
FILINFO info;
FIL fil;
char buf[128];
int ret;

// counters for ISR
volatile uint8_t gpsCounter = 0;
volatile uint8_t sdCounter = 0;
volatile uint8_t yprCounter = 0;
volatile uint8_t yprRateCounter = 0;

//
volatile bool is_at_1Hz = false;
volatile bool is_at_5Hz = false;
volatile bool is_at_20Hz = false;

// ypr values for sd and bluetooth
char str_y[12];
char str_p[12];
char str_r[12];
// gps vaues for SD
char str_lat[12];
char str_lon[12];
char str_spd[12];
char str_tsp[12];

// button setup
#define BUTTON_PRESSED (0 == (PIND & (1<<PORTD6)))
unsigned char currentButtonState = 0, previousButtonState = 0; // button states

// software serial - GPS
char c_soft_ser;
uint8_t soft_ser_counter = 0;
char GPS_BUFFER[82] = "$GPRMC,182331.00,A,5610.23167,N,01006.83445,E,0.414,,290519,,,A*77";

// data be-holder
FDLData flightData;

void timer1_init() {
	// set up timer with prescaler = 64 and CTC mode
	// TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
	TCCR1B |= (1 << WGM12)|(1 << CS12); // CTC, prescaler @ 256

	// initialize counter
	TCNT1 = 0;

	// initialize compare value at 10ms (100Hz)
	// 62499 (1 sec), 624 (10 ms)
	OCR1A = 624;

	TIMSK1 |= (1 << OCIE1A);
}

// WeirdLoop: my INT0
ISR (INT0_vect) {
	mpuInterrupt = true;
}

void i2cSetup() {
	// join I2C bus (I2Cdev library doesn't do this automatically)
	Fastwire::setup(400, true);

	ser = USARTWrapper::getInstance();
	ser->init(115200);
	// _delay_ms(1000); // add 1 second delay for connecting to the serial

	// ser->writeStringln("Serial setup done.");
}

void MPU6050Connect() {
	static int MPUInitCntr = 0;
	// initialize device
	mpu.initialize();
	// load and configure the DMP
	devStatus = mpu.dmpInitialize();

	if (devStatus != 0) {
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)

		const char* StatStr[5] { "No Error", "initial memory load failed",
				"DMP configuration updates failed", "3", "4" };

		MPUInitCntr++;

		ser->writeString("MPU/DMP connection failed: ");
		ser->writeStringln(StatStr[devStatus]);

		if (MPUInitCntr >= 10)
			return; //only try 10 times
		MPU6050Connect(); // lets try again
		return;
	}

	MPU6050_DMP_INT0SETUP; // setup the interrupt INT0

	// use predefined offsets for calibration
	mpu.setXAccelOffset(MPUOFFSETS[0]);
	mpu.setYAccelOffset(MPUOFFSETS[1]);
	mpu.setZAccelOffset(MPUOFFSETS[2]);
	mpu.setXGyroOffset(MPUOFFSETS[3]);
	mpu.setYGyroOffset(MPUOFFSETS[4]);
	mpu.setZGyroOffset(MPUOFFSETS[5]);

	// ser->writeStringln("Enabling the DMP...");
	mpu.setIntDMPEnabled(true); // enable the interrupt for the DMP
	mpu.setDMPEnabled(true);

	// get expected DMP packet size for later comparison
	packetSize = mpu.dmpGetFIFOPacketSize();
	mpu.resetFIFO(); // Clear FIFO buffer
	mpuInterrupt = false; // wait for next interrupt
}

/*
 * Get MPU/DMP data
 */
bool readMPUFIFOBuffer() {
	fifoCount = mpu.getFIFOCount();
	mpuInterrupt = false; // We caught an interrupt reset for next time

	/************************************
	 Check for incorrect packet size any size that is not divisible by 42 (Note: this will only occur when we overflow )
	 Check for overflow  (1024 bytes FiFo Buffer divided by 42 bytes Packet Size leaves a remainder of 16)
	 If the fifoCount is > Zero we have data so get it
	 ************************************/
	if ((!fifoCount) || (fifoCount % packetSize)) { // something's wrong. reset and try again.
		// wrong packet size, or overflow
		mpu.resetFIFO(); // clear the buffer and start over
		return (false); //no or corrupt data return false
	} else {
		/************************************
		 You will want to empty the FIFO buffer because the last packet is the latest reading
		 If more than one packet exists then the first packet is already at least 10 ms old!!!
		 ************************************/
		while (fifoCount >= packetSize) { // Get the packets until we have the latest!
			mpu.getFIFOBytes(fifoBuffer, packetSize); // lets do the magic and get the data
			fifoCount -= packetSize;
		}
		return (true);
	}
}

// get quaternion components in a [w, x, y, z] format
void getQuaternion() {
	mpu.dmpGetQuaternion(&flightData.q, fifoBuffer);
}

void printQuaternion() {
	// display quaternion values in easy matrix form: w x y z
	ser->writeDouble(flightData.q.x);
	ser->writeString(",");
	ser->writeDouble(flightData.q.y);
	ser->writeString(",");
	ser->writeDouble(flightData.q.z);
	ser->writeString(",");
	ser->writeDoubleln(flightData.q.w);
}

void getYawPitchRoll() {
	mpu.dmpGetQuaternion(&flightData.q, fifoBuffer);
	mpu.dmpGetGravity(&flightData.gravity, &flightData.q);
	mpu.dmpGetYawPitchRoll(flightData.ypr, &flightData.q, &flightData.gravity);

	// convert ypr to degrees from radians
	flightData.ypr[0] = flightData.ypr[0] * 180 / M_PI;
	flightData.ypr[1] = flightData.ypr[1] * 180 / M_PI;
	flightData.ypr[2] = flightData.ypr[2] * 180 / M_PI;
}
void printYawPitchRoll() {
	// display yaw, pitch, roll in degrees
	ser->writeDouble(flightData.ypr[0]);
	ser->writeString(",");
	ser->writeDouble(flightData.ypr[1]);
	ser->writeString(",");
	ser->writeDoubleln(flightData.ypr[2]);
}

/**
 * Send quaternion, YPR data, turn rate
 */
void printQuatAndYPRAndGPS() {
	ser->writeDouble(flightData.q.x);
	ser->writeString(",");
	ser->writeDouble(flightData.q.y);
	ser->writeString(",");
	ser->writeDouble(flightData.q.z);
	ser->writeString(",");
	ser->writeDouble(flightData.q.w);
	ser->writeString(",");
	ser->writeDouble(flightData.ypr[0]);
	ser->writeString(",");
	ser->writeDouble(flightData.ypr[1]);
	ser->writeString(",");
	ser->writeDouble(flightData.ypr[2]);
	ser->writeString(",");
	ser->writeDouble(yaw_rate);
	ser->writeString(",");
	ser->writeDouble(flightData.getLat());
	ser->writeString(",");
	ser->writeDouble(flightData.getLon());
	ser->writeString(",");
	ser->writeDouble(flightData.getSpeed());
	ser->writeString(",");
	ser->writeDoubleln(flightData.getTs());
}

void processGPSData() {
	switch (minmea_sentence_id(GPS_BUFFER, false)) {
		case MINMEA_SENTENCE_RMC: {
			struct minmea_sentence_rmc frame;
			if (minmea_parse_rmc(&frame, GPS_BUFFER)) {
				flightData.setLat(minmea_tocoord(&frame.latitude));
				flightData.setLon(minmea_tocoord(&frame.longitude));
				flightData.setSpeed(minmea_tocoord(&frame.speed));
				flightData.setTs(flightData.getUnixTime(frame.date.year, frame.date.month, frame.date.day,
						   frame.time.hours, frame.time.minutes, frame.time.seconds));

				// ser->writeDoubleln(flightData.getLat());
				// ser->writeDoubleln(flightData.getLon());
				// ser->writeDoubleln(flightData.getSpeed());
				// ser->writeLongln(flightData.getTs());
			} else {
				ser->sendChars("$xxRMC sentence is not parsed\n");
			}
		} break;

		case MINMEA_INVALID: {
			ser->sendChars("GPS sentence is not valid\n");
		} break;

		default: {
			ser->sendChars("GPS sentence is not parsed\n");
		} break;
	}
}

void exec_at_1Hz() {
	PIND |= (1 << DDD7); // toggle LED

	// write to SD card GPS data
	dtostrf(flightData.getLat(), 10, 6, str_lat);
	dtostrf(flightData.getLon(), 10, 6, str_lon);
	dtostrf(flightData.getSpeed(), 10, 6, str_spd);
	dtostrf(flightData.getTs(), 10, 0, str_tsp);
	sprintf( buf, "GPS:%s,%s,%s, %s\n", str_lat, str_lon, str_spd, str_tsp);
	ret = f_puts( buf, &fil );
	if ( ret == EOF ) {
	} else {
	}

	// handle IMU locking by resetting it
	if (quaternion_previous[0] == flightData.q.x &&
			quaternion_previous[1] == flightData.q.y &&
			quaternion_previous[2] == flightData.q.z &&
			quaternion_previous[3] == flightData.q.w ) {
		// we're stuck, restart the MPU6050 AND the i2c connection
		Fastwire::setup(400, true);
		MPU6050Connect();
		// _delay_ms(500);
	}

	quaternion_previous[0] = flightData.q.x;
	quaternion_previous[1] = flightData.q.y;
	quaternion_previous[2] = flightData.q.z;
	quaternion_previous[3] = flightData.q.w;

	/*if (roll_previous == flightData.getRoll()) {
		mpu.resetDMP();
		// MPU6050Connect();
	}

	roll_previous = flightData.getRoll();*/


#if FDL_MODE == FDL_DEMO
	processGPSData();
#endif

#if FDL_MODE == FDL_PRODUCTION
	// send via BT
	ser->sendMessage(MSG_LAT, (int16_t)(flightData.getLat()*100));
	ser->sendMessage(MSG_LON, (int16_t)(flightData.getLon()*100));
	ser->sendMessage(MSG_SPEED, (int16_t)(flightData.getSpeed())); // knots
#endif

#if FDL_MODE == FDL_PRODUCTION
	while ( (c_soft_ser = RxByte() )  ) {
		if (c_soft_ser) {
			if (c_soft_ser == '\r') {
				GPS_BUFFER[soft_ser_counter++] = '\0';
// ----------------------------------------------------------------------------
				processGPSData();
// ----------------------------------------------------------------------------
				// ser->sendChars(GPS_BUFFER);
				soft_ser_counter = 0;
				break;
			} else {
				GPS_BUFFER[soft_ser_counter++] = c_soft_ser;
			}
		}
	}
#endif
}

void exec_at_5Hz() {
#if FDL_MODE == FDL_DEMO
#endif
	// write to SD card
	dtostrf(flightData.ypr[3], 10, 6, str_y);
	dtostrf(flightData.ypr[1], 10, 6, str_p);
	dtostrf(flightData.ypr[2], 10, 6, str_r);
	sprintf( buf, "YPR:%s,%s,%s\n", str_y, str_p, str_r);
	ret = f_puts( buf, &fil );
	if ( ret == EOF ) {
	} else {
	}
}

void exec_at_20Hz() {
	yaw_rate = flightData.ypr[0] - yaw_previous[yprRateCounter];
	yaw_previous[yprRateCounter] = flightData.ypr[0];
	yprRateCounter++;

#if FDL_MODE == FDL_DEMO
	printQuatAndYPRAndGPS(); // send quaternion, YPR, yaw turn rate, and GPS data.
#endif

#if FDL_MODE == FDL_PRODUCTION
	// send bluetooth messages following the provided ICD
	ser->sendMessage(MSG_PITCH, (int)(flightData.ypr[1]*100));
	ser->sendMessage(MSG_ROLL, (int)(flightData.ypr[2]*100));
	ser->sendMessage(MSG_YAW_RATE, (int)(yaw_rate*100));
	ser->sendLongMessage(MSG_TIME, (uint64_t)(flightData.getTs())); // it should be 4 bytes
#endif
}

void setup_interfaces() {
	i2cSetup();
	MPU6050Connect();

	// set up SPI for the SD card
	/* PortB  +--------7: Pullup : non use  */
	/*        |+-------6: Pullup : non use  */
	/*        ||+------5: HIZ    : SD SCK   */
	/*        |||+-----4: HIZ    : SD MISO  */
	/*        ||||+----3: HIZ    : SD MOSI  */
	/*        |||||+---2: Out(H) : SD #CS   */
	/*        ||||||+--1: Pullup : non use  */
	/*        |||||||+-0: Pullup : non use	*/
	DDRB  = 0b00000100;
	PORTB = 0b11000111;

	// mount the SD card
	res = f_mount( 0 ,&fs );								/* re mount			*/
	if ( res != FR_OK ) {
		// printf( "f_mount() : error %u\n\n" ,res );
	} else {
		// printf( "f_mount() successful, code: %u\n\n" ,res );
	}
	res = f_open( &fil ,"/weird.txt", FA_CREATE_ALWAYS | FA_WRITE);
	if ( res != FR_OK ) {
		// printf( "f_open() : error %u\n\n" ,res );
	} else {
		// printf( "f_open() : file created - %u\n\n" ,res );
	}

	// initialize the buffer for the serial GPS
#if FDL_MODE == FDL_PRODUCTION
	for (int i=0; i<82; i++) {
		GPS_BUFFER[i] = '0';
	}
#endif

}

void logic_loop() {
	// ---- read from the FIFO buffer ---- //
	// The interrupt pin could have changed for some time already.
	// So set mpuInterrupt to false and wait for the next interrupt pin CHANGE signal.
	//  Wait until the next interrupt signal. This ensures the buffer is read right after the signal change.
	if (mpuInterrupt) {
		if (readMPUFIFOBuffer()) {
			// Calculate variables of interest using the acquired values from the FIFO buffer
			getQuaternion();
			getYawPitchRoll();

			// manipulate the MPU6050 readings
			// printQuatAndYPR();
		}
	}

	// functions to execute at sample rate X
	if (is_at_1Hz) {
		exec_at_1Hz();
		is_at_1Hz = false;
	}

	if (is_at_5Hz) {
		exec_at_5Hz();
		is_at_5Hz = false;
	}

	if (is_at_20Hz) {
		exec_at_20Hz();
		is_at_20Hz = false;
	}
}

ISR (TIMER1_COMPA_vect) {
	// FatFS needs to run this every 10ms
	disk_timerproc();

	// get and print quaternion data every 10 ms
	// printQuatAndYPR();
	// interrupt at 1000 ms: GPS
	if ( (gpsCounter % RATE_1) == 0) {
		gpsCounter = 0;
		yprRateCounter = 0; // reset the yaw rate counter every second
		is_at_1Hz = true;
	}

	// interrupt at 200 ms: SD card
	if ( (sdCounter % RATE_5) == 0) {
		sdCounter = 0;
		is_at_5Hz = true;
	}

	// interrupt at 50 ms: ypr
	if ( (yprCounter % RATE_20) == 0 ) {
		yprCounter = 0;
		is_at_20Hz = true;
	}

	gpsCounter++;
	sdCounter++;
	yprCounter++;
}


int main(void) {

	timer1_init();

	// setup LED
	DDRD |= (1 << DDD7);
	// button stuff (set as input pullup)
	DDRD &= ~(1 << DDD6);
	PORTD |= (1 << DDD6);

	setup_interfaces();

	while (1) {
		currentButtonState = BUTTON_PRESSED;
		if (currentButtonState != 0 && previousButtonState == 0) {
			f_close( &fil ); // close the file - remove SD card fast
			_delay_ms(3000); // artificial delay to remove sd card
			break;
		}
		logic_loop();

		previousButtonState = currentButtonState;
	}

	// f_close( &fil ); // never really reached, but just in case...

	return 0;
}

