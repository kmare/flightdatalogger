/*
 * USARTWrapper.h
 *
 *  Created on: May 18, 2019
 *      Author: pioann
 */

#ifndef USARTWRAPPER_H_
#define USARTWRAPPER_H_

#include "USART/usart.h"

// just making the g++ linker happy here... move along
__extension__ typedef int __guard __attribute__((mode (__DI__)));
extern "C" int __cxa_guard_acquire(__guard *);
extern "C" void __cxa_guard_release (__guard *);
extern "C" void __cxa_guard_abort (__guard *);
// end happiness

class USARTWrapper {
private:
	USARTWrapper();
public:
	// ~USARTWrapper() {};
	static USARTWrapper* getInstance();

	void init(uint32_t baudrate);

	void writeInt(uint8_t counter);
	void writeLong(uint32_t counter);
	void writeDouble(double counter);
	void writeString(const char* str);

	void writeIntln(uint8_t counter);
	void writeLongln(uint32_t counter);
	void writeDoubleln(double counter);
	void writeStringln(const char* str);
	void writeHEXln(uint8_t data);

	void sendMessage(unsigned char id, int16_t payload);
	void sendLongMessage(unsigned char id, uint64_t payload);
	void sendChar(char bf);
	void sendChars(const char *bf);
};

#endif /* USARTWRAPPER_H_ */
