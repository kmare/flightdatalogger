/*
 * USARTWrapper.cpp
 *
 *  Created on: May 18, 2019
 *      Author: pioann
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include "USARTWrapper.h"

// just making the g++ linker happy here... move along
int __cxa_guard_acquire(__guard *g) {return !*(char *)(g);};
void __cxa_guard_release (__guard *g) {*(char *)g = 1;};
void __cxa_guard_abort (__guard *) {};
// end happiness

USARTWrapper::USARTWrapper() {

}

USARTWrapper* USARTWrapper::getInstance() {
	static USARTWrapper instance;
	return &instance;
}

void USARTWrapper::init(uint32_t baudrate) {
	//uart_set_FrameFormat(USART_8BIT_DATA|USART_1STOP_BIT|USART_NO_PARITY|USART_ASYNC_MODE); // default settings
	uart_init(BAUD_CALC(baudrate)); // 8n1 transmission is set as default

	stdout = &uart0_io; // attach uart stream to stdout & stdin
	stdin = &uart0_io; // uart0_in and uart0_out are only available if NO_USART_RX or NO_USART_TX is defined

	sei(); // enable interrupts, library wouldn't work without this
}

void USARTWrapper::writeInt(uint8_t counter) {
	uart_putint(counter);
}

void USARTWrapper::writeLong(uint32_t counter) {
	uart_putulong(counter);
}

void USARTWrapper::writeDouble(double counter) {
	uart_putfloat(counter);
}

void USARTWrapper::writeString(const char* str) {
	printf(str);
}

void USARTWrapper::writeIntln(uint8_t counter) {
	uart_putint(counter);
	printf("\n");
}

void USARTWrapper::writeLongln(uint32_t counter) {
	uart_putulongr(counter, 10);
	printf("\n");
}

void USARTWrapper::writeDoubleln(double counter) {
	uart_putfloat(counter);
	printf("\n");
}

void USARTWrapper::writeStringln(const char* str) {
	printf(str);
	printf("\n");
}

void USARTWrapper::writeHEXln(uint8_t data) {
	uart_puthex(data);
	printf("\n");
}

void USARTWrapper::sendMessage(unsigned char id, int16_t payload) {
	uart_puthex(id); // uint8_t - 1 byte
	uart_putint(payload); // int16_t - 2 bytes
	printf("\n");
}

void USARTWrapper::sendLongMessage(unsigned char id, uint64_t payload) {
	uart_puthex(id); // uint8_t - 1 byte
	uart_putlong(payload); // int16_t - 2 bytes
	printf("\n");
}

void USARTWrapper::sendChar(char bf) {
	uart_putc(bf); // write array string to usart buffer
}

void USARTWrapper::sendChars(const char *bf) {
	while ( *bf )
		uart_putc(*bf++);
	uart_putc('\r');
	uart_putc('\n');
}
