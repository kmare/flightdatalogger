/*
 * config.h
 *
 *  Created on: May 30, 2019
 *      Author: pioann
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#ifdef F_CPU
#undef F_CPU
#define F_CPU 16000000UL
#endif

// set the running mode of the FDL
#ifndef FDL_MODE
#define FDL_MODE FDL_DEMO
// #define FDL_MODE FDL_PRODUCTION
#endif

#define FDL_DEMO 1
#define FDL_PRODUCTION 2

// MPU custom offsets
int MPUOFFSETS[6] = { -802, 3393, 1059, 17, -23, -32 };


#endif /* CONFIG_H_ */
