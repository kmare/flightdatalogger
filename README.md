A flight data logger for the atmega328. Supports the MPU6050 through I2C, hardware and software USART, SD card through SPI.
